<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
  </head>
  <body>
    <a href="../">Regresar</a>
    <hr />
    <h1>Documentación de API</h1>
    <p>
      La API se basa en los principios de REST y expone los datos de blah blah blah...
    </p>
    <p>
      La URL raiz es <a href="http://quirozchavezvalentina.sun.fire/api/">http://quirozchavezvalentina.sun.fire/api/</a>.
    </p>
    <hr />

    <h2>Obtener Xs</h2>
    <p>Descripción: blah blah blah...</p>
    <ul>
      <li><strong>URL</strong>: http://quirozchavezvalentina.sun.fire/api/xs</li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: ...</li>
        <li>b: ...</li>
      </ul>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
...
...
...
</pre>
      </li>
    </ul>

    <h2>Obtener x de Xs</h2>
    <p>Descripción: blah blah blah...</p>
    <ul>
      <li><strong>URL</strong>: http://quirozchavezvalentina.sun.fire/api/xs/[id]</li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>[id]: El x de Xs a obtener</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro es incorrecto se responde con el
        código de estado HTTP 000 y un mensaje de error indicando blah blah blah</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
...
...
...
</pre>
      </li>
    </ul>


  </body>
</html>
