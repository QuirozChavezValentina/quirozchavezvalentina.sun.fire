<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => '/',
    'recursos' => array(
      array('url' => '/api', 'descripcion' => 'Este documento HTML'),
      array('url' => '/api/xml', 'descripcion' => 'Un documento XML'),
    )
  );
  $app->render('root.php', $datos);
});
