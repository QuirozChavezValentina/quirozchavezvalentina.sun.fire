<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'books' => array(
      array(
        'id' => "bk101",
        'author' => "Gambardella, Matthew",
        'title' => "XML Developer's Guide",
        'genre' => "Computer",
        'price' => "44.95",
        'publish_date' => "2000-10-01",
        'description' => "An in-depth look at creating applications with XML."
      ),
      array(
        'id' => 'bk102',
        'author' => "Ralls, Kim",
        'title' => "Midnight Rain",
        'genre' => "Fantasy",
        'price' => "5.95",
        'publish_date' => "2000-12-16",
        'description' => "A former architect battles corporate zombies."
      )
    )
  );
  $app->render('xml.php', $datos);
});
